CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------
As a sub-module this module works only with the module Yild
(https://www.drupal.org/project/yild).
This module provides PoolParty Semantic Suite integration for multiple
Knowledge Graphs in the form of SKOS thesauri for Yild.

With this module you can tag your Drupal content with terms (concepts) out of
different SKOS SPARQL endpoints with one lookup.


REQUIREMENTS
------------
 * The module Yild (https://www.drupal.org/project/yild).
 * cURL needs to be installed on the web server your Drupal instance runs on.


INSTALLATION
------------
Install and enable first the modules from the "Requirements" list above and
then the Yild Poolparty Provider module as you would normally install a
contributed Drupal module. See: 
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------
Manage the different PoolParty settings at /admin/config/yild/poolparty.


MAINTAINERS
-----------
Current maintainer:
 * Kurt Moser (moserk) - https://www.drupal.org/u/moserk
