/**
 * @file
 * Handles AJAX submission to check availability of all SPARQL Endpoints.
 */

(function ($) {
  Drupal.behaviors.yild_poolparty = {
    attach: function (context) {

      $("div.yild-poolparty-led").each(function() {
        var $this = $(this);
        var url = Drupal.settings.basePath + "admin/config/yild/poolparty/" + $this.data("key") + "/available";
        $.get(url, function(data) {
          if (data == 1) {
            var led = "led-green";
            var title = Drupal.t("SPARQL Endpoint available");
          }
          else {
            var led = "led-red";
            var title = Drupal.t("SPARQL Endpoint NOT available");
          }
          $this.addClass(led);
          $this.attr("title", title);
        });
      });

    }
  };
})(jQuery);
