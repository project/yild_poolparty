<?php
/**
 * @file
 * PoolParty configuration UI for Yild PoolParty.
 */

/**
 * Ajax callback function for checking if a SPARQL Endpoint is available.
 *
 * @param array $setting
 *   A Yild Poolparty admin setting.
 *
 * @retrun boolean
 *   TRUE if SPARQL Endpoint is available otherwise FALSE.
 */
function yild_poolparty_admin_settings_available(array $setting) {
  $endpoint = new YildPoolpartySparqlService($setting);
  echo $endpoint->available() ? 1 : 0;
  exit();
}

/**
 * Ajax callback function for checking if a new SPARQL Endpoint is available.
 */
function yild_poolparty_admin_settings_new_available($form, $form_state) {
  $available = '<div id="health_info" class="available"><div class="semantic-connector-led led-green" title="Service available"></div>' . t('The SPARQL Endpoint is available.') . '</div>';
  $not_available = '<div id="health_info" class="not-available"><div class="semantic-connector-led led-red" title="Service NOT available"></div>' . t('The SPARQL Endpoint is not available or the credentials are incorrect.') . '</div>';

  if (isset($form_state['values']['url']) && valid_url($form_state['values']['url'], TRUE)) {
    $setting = array(
      'url' => $form_state['values']['url'],
      'username' => $form_state['values']['username'],
      'password' => $form_state['values']['password'],
    );
    $endpoint = new YildPoolpartySparqlService($setting);
    return $endpoint->available() ? $available : $not_available;
  }

  return $not_available;
}

/**
 * List all saved settings.
 *
 * @return string
 *   The rendered HTML of the list of settings.
 */
function yild_poolparty_admin_settings_list() {
  $settings = yild_poolparty_settings_load();

  $rows = array();
  foreach ($settings as $key => $setting) {
    $operations = array(
      l(t('Edit'), 'admin/config/yild/poolparty/' . $key . '/edit'),
      l(t('Delete'), 'admin/config/yild/poolparty/' . $key . '/delete'),
    );
    $image_attributes = array(
      'path' => 'misc/' . ($setting['enabled'] ? 'message-16-ok.png' : 'message-16-error.png'),
      'alt' => $setting['enabled'] ? 'enabled' : 'disabled',
      'title' => 'Configuration ' . ($setting['enabled'] ? 'enabled' : 'disabled'),
    );
    $rows[] = array(
      check_plain($setting['name']),
      check_url($setting['url']),
      '<div class="yild-poolparty-led" data-key="' . $key . '" title="Checking SPARQL Endpoint"></div>',
      theme('image', $image_attributes),
      implode(' | ', $operations),
    );
  }
  $table = array(
    '#theme' => 'table',
    '#header' => array(
      t('Name'),
      t('URL to SPARQL Endpoint'),
      t('Available'),
      t('Enabled'),
      t('Operations'),
    ),
    '#rows' => $rows,
    '#attached' => array(
      'js' => array(
        drupal_get_path('module', 'yild_poolparty') . '/js/yild_poolparty.admin.js',
      ),
      'css' => array(
        drupal_get_path('module', 'yild_poolparty') . '/css/yild_poolparty.admin.css',
      ),
    ),
  );

  return $table;
}

/**
 * Form constructor for the Yild Poolparty settings form.
 *
 * @param array $setting
 *   The Yild Poolparty setting if it is not null.
 *
 * @see yild_poolparty_form_validate()
 * @see yild_poolparty_form_submit()
 *
 * @ingroup forms
 */
function yild_poolparty_admin_settings_form($form, &$form_state, array $setting = NULL) {
  if (is_null($setting)) {
    $new_setting = TRUE;
    $setting = array(
      'name' => '',
      'url' => '',
      'username' => '',
      'password' => '',
      'enabled' => TRUE,
    );
  }
  else {
    $new_setting = FALSE;
    $form['setting_key'] = array(
      '#type' => 'hidden',
      '#value' => _yild_poolparty_create_key($setting['name']),
    );
  }

  $form['yild_poolparty_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yild PoolParty configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['yild_poolparty_settings']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Name of this setting (max. 12 characters). Is shown in the autocomplete drop-down list.'),
    '#size' => 35,
    '#maxlength' => 12,
    '#default_value' => $setting['name'],
    '#required' => TRUE,
  );
  $form['yild_poolparty_settings']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Configuration enabled'),
    '#description' => t('Check it if you want that the service to be used.'),
    '#default_value' => $setting['enabled'],
  );
  $form['yild_poolparty_settings']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('The URL where the SKOS SPARQL Endpoint runs.'),
    '#size' => 100,
    '#maxlength' => 255,
    '#default_value' => $setting['url'],
    '#required' => TRUE,
  );
  $form['yild_poolparty_settings']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Username for the credentials.'),
    '#size' => 35,
    '#maxlength' => 60,
    '#default_value' => $setting['username'],
  );
  $form['yild_poolparty_settings']['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password for the credentials.'),
    '#size' => 35,
    '#maxlength' => 128,
    '#default_value' => $setting['password'],
  );
  $form['yild_poolparty_settings']['health_check'] = array(
    '#type' => 'button',
    '#value' => t('Health check'),
    '#ajax' => array(
      'callback' => 'yild_poolparty_admin_settings_new_available',
      'wrapper' => 'health_info',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );
  if ($new_setting) {
    $markup = '<div id="health_info">' . t('Click to check if the SPARQL Endpoint is available.') . '</div>';
  }
  else {
    $endpoint = new YildPoolpartySparqlService($setting);
    $available = '<div id="health_info" class="available"><div class="semantic-connector-led led-green" title="Service available"></div>' . t('The SPARQL Endpoint is available.') . '</div>';
    $not_available = '<div id="health_info" class="not-available"><div class="semantic-connector-led led-red" title="Service NOT available"></div>' . t('The SPARQL Endpoint is not available or the credentials are incorrect.') . '</div>';
    $markup = $endpoint->available() ? $available : $not_available;
  }
  $form['yild_poolparty_settings']['health_info'] = array(
    '#markup' => $markup,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/yild/poolparty',
  );

  return $form;
}

/**
 * The validation handler for yild_poolparty_admin_settings_form().
 *
 * @see yild_poolparty_form_submit()
 */
function yild_poolparty_admin_settings_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'], TRUE)) {
    form_set_error('url', t('%field must be a valid URL.', array('%field' => $form_state['values']['url'])));
  }
}

/**
 * The submission handler for yild_poolparty_admin_settings_form().
 *
 * @see yild_poolparty_form_validate()
 */
function yild_poolparty_admin_settings_form_submit($form, &$form_state) {
  $settings = yild_poolparty_settings_load();
  if (isset($form_state['values']['setting_key'])) {
    unset($settings[$form_state['values']['setting_key']]);
  }
  $key = _yild_poolparty_create_key($form_state['values']['name']);
  $settings[$key] = array(
    'name' => $form_state['values']['name'],
    'url' => $form_state['values']['url'],
    'username' => $form_state['values']['username'],
    'password' => $form_state['values']['password'],
    'enabled' => $form_state['values']['enabled'],
  );
  variable_set('yild_poolparty_settings', $settings);
  drupal_set_message(t('The PoolParty setting %name has been saved.', array('%name' => $form_state['values']['name'])));
  $form_state['redirect'] = 'admin/config/yild/poolparty';
}

/**
 * The confirmation form constructor for deleting a Yild Poolparty setting.
 *
 * @param string $setting_name
 *   The name of a Yild Poolparty setting.
 *
 * @see yild_poolparty_admin_settings_delete_form_submit()
 */
function yild_poolparty_admin_settings_delete_form($form, &$form_state, $setting_name) {
  $form_state['setting_name'] = $setting_name;
  return confirm_form($form,
    t('Are you sure you want to delete the Yild PoolParty setting %name?', array('%name' => $setting_name)),
    'admin/config/yild/poolparty',
    t('This action cannot be undone.'),
    t('Delete setting'));
}

/**
 * The submit handler confirmation form for deleting a Yild Poolparty setting.
 */
function yild_poolparty_admin_settings_delete_form_submit($form, &$form_state) {
  $settings = yild_poolparty_settings_load();
  $setting_name = $form_state['setting_name'];
  unset($settings[$setting_name]);

  variable_set('yild_poolparty_settings', $settings);
  drupal_set_message(t('The PoolParty setting %name has been deleted.', array('%name' => $setting_name)));
  $form_state['redirect'] = 'admin/config/yild/poolparty';
}
