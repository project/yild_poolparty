<?php
/**
 * @file
 * The class for the communication with the SPARQL Endpoint.
 */

/**
 * Class YildPoolpartySparqlService definition.
 *
 * It communicates with a defined SPARQL Endpoint.
 */
class YildPoolpartySparqlService {

  protected $setting;

  /**
   * The constructor.
   *
   * @param array $setting
   *   The Yild Poolparty setting.
   */
  public function __construct(array $setting = NULL) {
    $this->setting = $setting;
  }

  /**
   * Sets a new setting array.
   *
   * @param array $setting
   *   The Yild Poolparty setting.
   */
  public function setSetting(array $setting) {
    $this->setting = $setting;
  }

  /**
   * Checks if a SPARQL Endpoint is running or not.
   *
   * @return bool
   *   TRUE if SPARQL Endpoint is available otherwise FALSE.
   */
  public function available() {
    $query = "
    PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
    SELECT ?concept
    WHERE {
      ?concept a skos:Concept.
    }
    LIMIT 1";
    $result = $this->call($query);
    return !is_null($result) ? TRUE : FALSE;
  }

  /**
   * Searches concepts with a SPARQL Query.
   *
   * @param string $search_string
   *   The search string.
   * @param string $lang
   *   The language.
   *
   * @return array
   *   The result from the SPARQL Endpoint if available
   *   otherwise an empty array.
   */
  public function search($search_string, $lang) {
    $query = "
    PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
    SELECT DISTINCT ?concept ?prefLabel ?broaderLabel
    WHERE {
      { ?concept skos:prefLabel ?prefLabel FILTER(regex(str(?prefLabel),'$search_string','i') && lang(?prefLabel) = '$lang'). }
      UNION
      { ?concept skos:altLabel ?prefLabel FILTER(regex(str(?prefLabel),'$search_string','i') && lang(?prefLabel) = '$lang'). }
      OPTIONAL {
       ?concept skos:broader ?broader.
       ?broader skos:prefLabel ?broaderLabel FILTER(lang(?broaderLabel) = '$lang').
      }
    }
    ORDER BY ASC(?prefLabel)
    LIMIT " . YILD_POOLPARTY_MAX_HITS;

    $result = $this->call($query, TRUE);

    return !is_null($result) ? $result : array();
  }

  /**
   * Calls the SPARQL Endpoint with a given query.
   *
   * @param string $query
   *   The SPARQL query.
   * @param bool $caching
   *   If true caches the result, otherwise not.
   *
   * @return array|null
   *   The result from the SPARQL Endpoint if available, otherwise NULL.
   */
  protected function call($query, $caching = FALSE) {
    $uri = $this->setting['url'] . '?query=' . urlencode($query) . '&format=application/json';

    $result = NULL;
    if ($caching) {
      $cache = $this->getCache($uri);
      if (!empty($cache)) {
        $result = $cache;
      }
    }

    if (is_null($result)) {
      $ch = curl_init($uri);
      if (!empty($this->setting['username'])) {
        $credentials = $this->setting['username'] . ':' . $this->setting['password'];
        curl_setopt($ch, CURLOPT_USERPWD, $credentials);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
      }
      curl_setopt($ch, CURLOPT_TIMEOUT, 3);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));

      $result = curl_exec($ch);
      $error_code = curl_error($ch);

      if ($error_code == 0) {
        $result = drupal_json_decode($result);
        if ($caching) {
          $this->putCache($uri, $result);
        }
      }

      curl_close($ch);
    }

    return $result;
  }

  /**
   * Retrieves a search result from Drupal's internal cache.
   *
   * @param string $cid
   *   The cache id to use.
   *
   * @return bool
   *   The cached data.
   */
  protected function getCache($cid) {
    $cache = cache_get($cid, 'yild-cache');
    if (!empty($cache)) {
      if ($cache->expire > time()) {
        return $cache->data;
      }
    }
    return FALSE;
  }

  /**
   * Saves a search to Drupal's internal cache.
   *
   * @param string $cid
   *   The cache id to use.
   * @param object $data
   *   The data to cache.
   */
  protected function putCache($cid, $data) {
    cache_set($cid, $data, 'yild-cache', time() + YILD_POOLPARTY_CACHE_LIFETIME);
  }

}
